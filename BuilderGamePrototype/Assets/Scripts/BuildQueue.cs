﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.Assertions;

public class BuildQueue : MonoBehaviour {

    [SerializeField]
    private List<Worker> workers = new List<Worker>();
    private List<Worker> stupidWorkers = new List<Worker>();
    [SerializeField]
    private GameObject workerPrefab;

    private List<BuildTileData> dataQueue = new List<BuildTileData>();

    private int skipWorkerCount;
    private bool willSkipWorker;

	// Use this for initialization
	void Start () {
        Assert.IsNotNull(this.workerPrefab);

        Messenger.AddListener<BuildTileData>(Messages.START_BUILDING, OnStartBuilding);
        Messenger.AddListener<BuildTileData>(Messages.BUILD_COMPLETE, OnBuildComplete);
        Messenger.AddListener<BuildTileData>(Messages.BUILD_FAIL, OnBuildFail);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener<BuildTileData>(Messages.START_BUILDING, OnStartBuilding);
        Messenger.RemoveListener<BuildTileData>(Messages.BUILD_COMPLETE, OnBuildComplete);
        Messenger.RemoveListener<BuildTileData>(Messages.BUILD_FAIL, OnBuildFail);
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Alpha5))
        {
            GameObject wPrefab = Instantiate(this.workerPrefab, this.transform);
            Worker w = wPrefab.GetComponentInChildren<Worker>();
            this.workers.Add(w);
        }
    }

    private void OnStartBuilding(BuildTileData data)
    {
        //add to queue
        this.dataQueue.Add(data);
        ProcessQueue();
    }

    //on build complete or fail, check for next queue
    private void OnBuildComplete(BuildTileData data)
    {
        ProcessQueue();

    }

    private void OnBuildFail(BuildTileData data)
    {
        this.willSkipWorker = true;
        //TODO: try again with the failed data with the other workers before completely ignoring it
        ProcessQueue();
    }

    private void ProcessQueue()
    {
        if (this.dataQueue.Count == 0) { return; }

        foreach (Worker w in this.workers)
        {
            if (w.State == Worker.WorkerState.Idle)
            {
                if (willSkipWorker)
                {
                    this.willSkipWorker = false;
                    this.stupidWorkers.Add(w);
                    continue;
                }
                w.OnStartBuilding(this.dataQueue[0]);
                this.dataQueue.Remove(this.dataQueue[0]);
                break;
            }
        }

        this.workers.RemoveAll(i => this.stupidWorkers.Contains(i));
        this.workers.AddRange(this.stupidWorkers);
        this.stupidWorkers.Clear();
    }
}
