﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BuildTileData {

    private Vector3Int position;
    public Vector3Int Position
    {
        get
        {
            return this.position;
        }
        set
        {
            this.position = value;
        }
    }

    private TileBase tileBase;
    public TileBase TileBase
    {
        get
        {
            return this.tileBase;
        }
        set
        {
            this.tileBase = value;
            PreviewTile pTile = (PreviewTile)this.tileBase;
            this.tileBaseSprite = pTile.sprite;
        }
    }

    private Sprite tileBaseSprite;
    public Sprite TileBaseSprite
    {
        get
        {
            return this.tileBaseSprite;
        }
    }

}
