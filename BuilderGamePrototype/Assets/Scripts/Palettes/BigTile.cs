﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class BigTile : RotatableTile {

    [SerializeField]
    private Vector2Int size;
    public Vector2Int Size
    {
        get
        {
            return this.size;
        }
        set
        {
            this.size = value;
        }
    }

    // for building bigger tiles than 1x1. pivot is -1,-1 (lower left to upper right)
    [SerializeField]
    private ExcessSprites[] excessSprites;
    public ExcessSprites[] ExcessSprites
    {
        get { return this.excessSprites; }
    }
}
