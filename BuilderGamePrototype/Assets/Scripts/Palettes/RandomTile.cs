﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RandomTile : PreviewTile {

    [SerializeField]
    private Sprite[] randomSprites;

    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        base.GetTileData(position, tilemap, ref tileData);
        tileData.sprite = this.randomSprites[Random.Range(0, this.randomSprites.Length)];
    }
}
