﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ExcessSprites", menuName = "ExcessSprites", order = 1)]

[System.Serializable]
public class ExcessSprites : ScriptableObject {

    [SerializeField]
    public List<Sprite> sprites;
}
