﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class RotatableTile : PreviewTile
{
    [SerializeField]
    private Sprite[] sprites;
    public Sprite[] Sprites
    {
        get
        {
            return this.sprites;
        }
        set
        {
            this.sprites = value;
        }
    }

    [SerializeField]
    private int rotationIndex;
    public int RotationIndex
    {
        get
        {
            return this.rotationIndex;
        }
        set
        {
            this.rotationIndex = value;
            this.PreviewSprite = this.sprites[this.rotationIndex];
        }
    }
}
