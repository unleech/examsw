﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedManager : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Alpha1))
        {
            Messenger.Broadcast(Messages.SPEED_UP, 1);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha2))
        {
            Messenger.Broadcast(Messages.SPEED_UP, 2);
        }
        else if (Input.GetKeyUp(KeyCode.Alpha3))
        {
            Messenger.Broadcast(Messages.SPEED_UP, 4);
        }
    }
}
