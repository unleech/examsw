﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class WallTile : PreviewTile {

    public float rotation;

    [SerializeField]
    private Sprite[] wallSprites;

    private int bitMaskValue;

    public override void RefreshTile(Vector3Int position, ITilemap tilemap)
    {
        base.RefreshTile(position, tilemap);
        CheckNeighbor(position, tilemap, true);
    }

    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        base.GetTileData(position, tilemap, ref tileData);
        CheckNeighbor(position, tilemap, false);

        tileData.sprite = this.wallSprites[this.bitMaskValue];
    }

    private void CheckNeighbor(Vector3Int position, ITilemap tilemap, bool willRefresh)
    {
        this.bitMaskValue = 0;

        Vector3Int nPos = position + Vector3Int.left;
        //check left
        if (HasWalls(tilemap, nPos))
        {
            if (willRefresh)
            {
                tilemap.RefreshTile(nPos);
            }
            this.bitMaskValue += 2;
        }

        nPos = position + Vector3Int.right;
        //check right
        if (HasWalls(tilemap, nPos))
        {
            if (willRefresh)
            {
                tilemap.RefreshTile(nPos);
            }
            this.bitMaskValue += 4;
        }

        nPos = position + Vector3Int.up;
        //check up
        if (HasWalls(tilemap, nPos))
        {
            if (willRefresh)
            {
                tilemap.RefreshTile(nPos);
            }
            this.bitMaskValue += 1;
        }

        nPos = position + Vector3Int.down;
        //check down
        if (HasWalls(tilemap, nPos))
        {
            if (willRefresh)
            {
                tilemap.RefreshTile(nPos);
            }
            this.bitMaskValue += 8;
        }
    }

    private bool HasWalls(ITilemap tilemap, Vector3Int position)
    {
        return tilemap.GetTile(position) == this;
    }
}
