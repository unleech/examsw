﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class PreviewTile : Tile {

    [SerializeField]
    private Sprite previewSprite;
    public Sprite PreviewSprite
    {
        get
        {
            return this.previewSprite;
        }
        set
        {
            this.previewSprite = value;
        }
    }
}
