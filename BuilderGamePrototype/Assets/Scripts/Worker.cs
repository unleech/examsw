﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Pathfinding;

public class Worker : AILerp {

    public enum WorkerState
    {
        Idle,
        Busy
    }

    [SerializeField]
    private WorkerState state;
    public WorkerState State
    {
        get
        {
            return this.state;
        }
        set
        {
            this.state = value;
        }
    }

    [SerializeField]
    private float buildTime = 3;
    private float origSpeed;
    private float origBuildTime;

    private BuildTileData currentData;
    private Vector3 expectedPos;

    protected override void Start()
    {
        base.Start();
        this.origBuildTime = this.buildTime;
        this.origSpeed = this.speed;

        Messenger.AddListener<int>(Messages.SPEED_UP, OnChangeSpeed);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener<int>(Messages.SPEED_UP, OnChangeSpeed);
    }

    public void OnStartBuilding(BuildTileData data)
    {
        if (this.state == WorkerState.Busy) { return; }
        this.state = WorkerState.Busy;
        UpdateTarget(data.Position);
        this.currentData = data;
    }

    private void UpdateTarget(Vector3Int position)
    {
        this.target.position = this.expectedPos = new Vector3(position.x + 0.5f, position.y + 0.5f, position.z);
        this.ForceSearchPath();
    }

    public override void OnTargetReached()
    {
        base.OnTargetReached();
        StartCoroutine(WaitAndBuild());
    }

    IEnumerator WaitAndBuild()
    {
        yield return null;

        if (IsReachable())
        {
            yield return new WaitForSeconds(this.buildTime);
            this.state = WorkerState.Idle;

            Messenger.Broadcast(Messages.BUILD_COMPLETE, this.currentData);
        }
        else
        {
            this.state = WorkerState.Idle;

            Messenger.Broadcast(Messages.BUILD_FAIL, this.currentData);
        }
    }

    private bool IsReachable()
    {
        if (this.transform.position == this.expectedPos)
        {
            return true;
        }

        return false;
    }

    private void OnChangeSpeed(int multiplier)
    {
        this.speed = this.origSpeed * multiplier;
        this.buildTime = this.origBuildTime / multiplier;
    }
}
