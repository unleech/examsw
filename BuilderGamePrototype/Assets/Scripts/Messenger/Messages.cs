﻿using UnityEngine;
using System.Collections;

public static class Messages
{
    public const string START_BUILDING = "StartBuilding";
    public const string BUILD_COMPLETE = "BuildComplete";
    public const string BUILD_FAIL = "BuildFail";

    public const string SELECT_TILE_BASE = "SelectTileBase";
    public const string ROTATE_TILE = "RotateTile";
    public const string SPEED_UP = "SpeedUp";
}