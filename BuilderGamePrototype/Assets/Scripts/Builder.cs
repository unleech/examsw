﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;
using UnityEngine.Assertions;
using System;

public class Builder : MonoBehaviour {

    [SerializeField]
    private EventSystem eventSystem;
    [SerializeField]
    private AstarPath aStarGrid;
    [SerializeField]
    private Grid grid;
    [SerializeField]
    private Tilemap tilemap;
    [SerializeField]
    private Tilemap wallmap;
    [SerializeField]
    private Tilemap objectmap;
    [SerializeField]
    private Tilemap buildingmap;
    [SerializeField]
    private Tilemap hovermap;
    [SerializeField]
    private TileBase selectedTileBase;

    List<Vector3Int> previousHoverSet = new List<Vector3Int>();
    private Vector3Int previousHoverGridPos;
    private Vector3Int gridPosOrigin;

    [SerializeField]
    private GameObject tempFixColliderBug;

    private BuildTileData data;

    // Use this for initialization
    void Start () {
        Assert.IsNotNull(this.eventSystem);
        Assert.IsNotNull(this.aStarGrid);
        Assert.IsNotNull(this.grid);
        Assert.IsNotNull(this.tilemap);
        Assert.IsNotNull(this.wallmap);
        Assert.IsNotNull(this.objectmap);
        Assert.IsNotNull(this.buildingmap);
        Assert.IsNotNull(this.hovermap);
        Assert.IsNotNull(this.selectedTileBase);

        Messenger.AddListener<BuildTileData>(Messages.BUILD_COMPLETE, OnBuildComplete);
        Messenger.AddListener<BuildTileData>(Messages.BUILD_FAIL, OnBuildFail);
        Messenger.AddListener<TileBase>(Messages.SELECT_TILE_BASE, OnSelectTileBase);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener<BuildTileData>(Messages.BUILD_COMPLETE, OnBuildComplete);
        Messenger.RemoveListener<BuildTileData>(Messages.BUILD_FAIL, OnBuildFail);
        Messenger.RemoveListener<TileBase>(Messages.SELECT_TILE_BASE, OnSelectTileBase);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.eventSystem.IsPointerOverGameObject())
        {
            return;
        }

        DisplayHoverGuide();

        //for dragging reference
        if (Input.GetMouseButtonDown(0))
        {
            this.gridPosOrigin = grid.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }
        
        if(Input.GetMouseButtonUp(0))
        { 
            Vector3 world = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3Int gridPos = grid.WorldToCell(world);

            if (this.selectedTileBase.GetType() == typeof(WallTile))
            {
                BuildWall(gridPos);
            }
            else if (this.selectedTileBase.GetType() == typeof(RotatableTile))
            {
                BuildTile(gridPos);
            }
            else if (this.selectedTileBase.GetType() == typeof(BigTile))
            {
                BuildBigTile(gridPos);
            }
        }

        //rotation
        if (Input.GetKeyUp(KeyCode.R) || Input.GetMouseButtonDown(2))
        {
            if (this.selectedTileBase.GetType() == typeof(RotatableTile) || this.selectedTileBase.GetType().BaseType == typeof(RotatableTile))
            {
                RotateTile();
            }
        }
    }

    //queue draggable wall and checks if it is horizontal or vertial from the click origin upto the release of the mouse button even if there is an obstacle in the middle of it
    private void BuildWall(Vector3Int gridPos)
    {
        Vector3Int difference = this.gridPosOrigin - gridPos;
        //if horizontal is longer or equal
        if (Mathf.Abs(difference.x) >= Mathf.Abs(difference.y))
        {
            if (difference.x == 0)
            {
                BuildTile(gridPos);
            }
            else if (difference.x > 0)
            {
                //queue wall from right to left
                for (int x = this.gridPosOrigin.x; x >= gridPos.x; x--)
                {
                    Vector3Int newPos = new Vector3Int(x, this.gridPosOrigin.y, this.gridPosOrigin.z);
                    BuildTile(newPos);
                }
            }
            else if (difference.x < 0)
            {
                //queue wall from left to right
                for (int x = this.gridPosOrigin.x; x <= gridPos.x; x++)
                {
                    Vector3Int newPos = new Vector3Int(x, this.gridPosOrigin.y, this.gridPosOrigin.z);
                    BuildTile(newPos);
                }
            }
        }
        else
        {
            if (difference.y == 0)
            {
                BuildTile(gridPos);
            }
            else if (difference.y > 0)
            {
                for (int y = this.gridPosOrigin.y; y >= gridPos.y; y--)
                {
                    Vector3Int newPos = new Vector3Int(this.gridPosOrigin.x, y, this.gridPosOrigin.z);
                    BuildTile(newPos);
                }
            }
            else if (difference.y < 0)
            {
                for (int y = this.gridPosOrigin.y; y <= gridPos.y; y++)
                {
                    Vector3Int newPos = new Vector3Int(this.gridPosOrigin.x, y, this.gridPosOrigin.z);
                    BuildTile(newPos);
                }
            }
        }
    }

    //TODO: check this in mouseOver
    private bool IsBuildable(Vector3Int position)
    {
        if(this.buildingmap.GetTile(position) == null && this.wallmap.GetTile(position) == null && this.objectmap.GetTile(position) == null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void RotateTile()
    {
        RotatableTile rTile = (RotatableTile)this.selectedTileBase;
        rTile.RotationIndex = (rTile.RotationIndex + 1) % rTile.Sprites.Length;
        rTile.sprite = rTile.Sprites[rTile.RotationIndex];

        Messenger.Broadcast(Messages.ROTATE_TILE);
    }

    private void BuildTile(Vector3Int gridPos)
    {
        if (IsBuildable(gridPos))
        {
            ChangeTileToSelected(gridPos);
        }
    }

    private void BuildBigTile(Vector3Int gridPos)
    {
        BigTile bTile = (BigTile)this.selectedTileBase;
        for (int y = 0; y < bTile.Size.y; y++)
        {
            for (int x = 0; x < bTile.Size.x; x++)
            {
                if (!IsBuildable(new Vector3Int(gridPos.x + x, gridPos.y + y, gridPos.z)))
                {
                    return;
                }
            }
        }

        int index = 0;
        for (int y = 0; y < bTile.Size.y; y++)
        {
            for (int x = 0; x < bTile.Size.x; x++)
            {
                bTile.sprite = bTile.ExcessSprites[bTile.RotationIndex].sprites[index];
                BuildTileData data = new BuildTileData();
                data.Position = new Vector3Int(gridPos.x + x, gridPos.y + y, gridPos.z);
                data.TileBase = bTile;
                ChangeTileToSelected(data);

                index++;
            }
        }
    }

    private void ChangeTileToSelected(Vector3Int position)
    {
        data = new BuildTileData();
        this.data.Position = position;
        this.data.TileBase = this.selectedTileBase;
        
        Messenger.Broadcast(Messages.START_BUILDING, this.data);
        this.buildingmap.SetTile(position, this.data.TileBase);       
    }

    private void ChangeTileToSelected(BuildTileData data)
    {
        Messenger.Broadcast(Messages.START_BUILDING, data);
        this.buildingmap.SetTile(data.Position, data.TileBase);
    }

    private void OnBuildComplete(BuildTileData data)
    {
        //depending on the selected tile base
        if(data.TileBase.GetType() == typeof(WallTile)) { 
            this.wallmap.SetTile(data.Position, data.TileBase);
            FixBugColliderBuild(data.Position);
        }
        else if(data.TileBase.GetType() == typeof(RotatableTile)) 
        {
            RotatableTile rTile = (RotatableTile)data.TileBase;
            rTile.sprite = data.TileBaseSprite;
            this.objectmap.SetTile(data.Position, rTile);
        }
        else if (data.TileBase.GetType() == typeof(BigTile))
        {
            BigTile bTile = (BigTile)data.TileBase;
            bTile.sprite = data.TileBaseSprite;
            this.objectmap.SetTile(data.Position, bTile);
        }

        this.buildingmap.SetTile(data.Position, null);

        Invoke("DelayScan", 0.1f);
    }

    private void DelayScan()
    {
        AstarPath.active.Scan();
        this.aStarGrid.Scan();
    }

    private void FixBugColliderBuild(Vector3Int position)
    {
        Instantiate(this.tempFixColliderBug, new Vector3(position.x + 0.5f, position.y + 0.5f, position.z), Quaternion.identity);
    }

    private void OnBuildFail(BuildTileData data)
    {
        this.buildingmap.SetTile(data.Position, null);
    }

    private void OnSelectTileBase(TileBase tileBase)
    {
        this.selectedTileBase = tileBase;
    }

    private void DisplayHoverGuide()
    {
        Vector3 world = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int gridPos = grid.WorldToCell(world);
        if (this.previousHoverGridPos == gridPos) { return; }

        //TODO: BigTile hover and Wall drag hover
        if (this.selectedTileBase.GetType() == typeof(BigTile))
        {

            int index = 0;
            BigTile bTile = (BigTile)this.selectedTileBase;


            foreach (Vector3Int v3 in this.previousHoverSet)
            {
                this.hovermap.SetTile(v3, null);
            }

            this.previousHoverSet.Clear();

            for (int y = 0; y < bTile.Size.y; y++)
            {
                for (int x = 0; x < bTile.Size.x; x++)
                {
                    Vector3Int newPos = new Vector3Int(gridPos.x + x, gridPos.y + y, gridPos.z);
                    previousHoverSet.Add(newPos);
                    bTile.sprite = bTile.ExcessSprites[bTile.RotationIndex].sprites[index];
                    BuildTileData data = new BuildTileData();
                    data.Position = new Vector3Int(gridPos.x + x, gridPos.y + y, gridPos.z);
                    data.TileBase = bTile;

                    this.hovermap.SetTile(newPos, data.TileBase);
                    Highlight(newPos);

                    index++;
                }
            }
            this.previousHoverGridPos = gridPos;

            return;
        }

        if (this.previousHoverGridPos != null)
        {
            this.hovermap.SetTile(previousHoverGridPos, null);
        }
        this.previousHoverGridPos = gridPos;

        this.hovermap.SetTile(gridPos, this.selectedTileBase);

        Highlight(gridPos);
    }

    private void Highlight(Vector3Int gridPos)
    {
        if (IsBuildable(gridPos))
        {
            this.hovermap.RemoveTileFlags(gridPos, TileFlags.LockColor);
            this.hovermap.SetColor(gridPos, Color.green);
        }
        else
        {
            this.hovermap.RemoveTileFlags(gridPos, TileFlags.LockColor);
            this.hovermap.SetColor(gridPos, Color.red);
        }
    }
}
