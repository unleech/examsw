﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CustomPalettes {

    [MenuItem("Assets/Create/Tiles/BigTile")]
    public static void CreateBigTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save BigTile", "New BigTile", "asset", "Save BigTile", "Assets");
        if (string.IsNullOrEmpty(path))
        {
            return;
        }

        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<BigTile>(), path);
    }

    [MenuItem("Assets/Create/Tiles/RandomTile")]
    public static void CreateRandomTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Randomtile", "New Randomtile", "asset", "Save Randomtile", "Assets");
        if (string.IsNullOrEmpty(path))
        {
            return;
        }

        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<RandomTile>(), path);
    }

    [MenuItem("Assets/Create/Tiles/RotatableTile")]
    public static void CreateRotatableTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save RotatableTile", "New RotatableTile", "asset", "Save RotatableTile", "Assets");
        if (string.IsNullOrEmpty(path))
        {
            return;
        }

        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<RotatableTile>(), path);
    }

    [MenuItem("Assets/Create/Tiles/WallTile")]
    public static void CreateWallTile()
    {
        string path = EditorUtility.SaveFilePanelInProject("Save Walltile", "New Walltile", "asset", "Save Walltile", "Assets");
        if (string.IsNullOrEmpty(path))
        {
            return;
        }

        AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<WallTile>(), path);
    }
}
