﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class TileBaseButton : MonoBehaviour {

    [SerializeField]
    private TileBase tileBase;
    [SerializeField]
    private Transform selected;
    [SerializeField]
    private Image preview;

    private void Start()
    {
        Assert.IsNotNull(this.tileBase);
        Assert.IsNotNull(this.selected);
        Assert.IsNotNull(this.preview);

        Messenger.AddListener(Messages.ROTATE_TILE, OnRotateTile);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener(Messages.ROTATE_TILE, OnRotateTile);
    }

    //called in inspector
    public void OnClick(bool flag)
    {
        Messenger.Broadcast(Messages.SELECT_TILE_BASE, this.tileBase);
        
        if(flag)
        {
            this.selected.SetParent(this.transform);
            this.selected.localPosition = Vector3.zero;
        }
    }

    private void OnRotateTile()
    {
        PreviewTile pTile = (PreviewTile)this.tileBase;
        this.preview.sprite = pTile.PreviewSprite;
    }
}
