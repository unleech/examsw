﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDrag : MonoBehaviour {

    private Vector3 dragOrigin;
    //TODO: add clipping
    private Vector4 clipping;

    // Update is called once per frame
    void Update()
    {
        /* Map/Camera controls */

        //zooming in/out
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (Camera.main.orthographicSize <= 3) { return; }
            Camera.main.orthographicSize -= 1;   
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (Camera.main.orthographicSize >= 16) { return; }
            Camera.main.orthographicSize += 1;  
        }

        //TODO: use LMB
        if (Input.GetMouseButtonDown(1))
        {
            this.dragOrigin = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
            this.dragOrigin = Camera.main.ScreenToWorldPoint(this.dragOrigin);
        }

        if (Input.GetMouseButton(1))
        {
            Vector3 currentPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
            currentPos = Camera.main.ScreenToWorldPoint(currentPos);
            Vector3 movePos = this.dragOrigin - currentPos;
            transform.position = transform.position + movePos;
        }

    }
}
